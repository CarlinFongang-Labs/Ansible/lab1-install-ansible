# Ansible | Installation d'Ansible sous Ubuntu


_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition
Ansible est un outil d'automatisation informatique qui simplifie la configuration de serveurs, le déploiement d'applications et la gestion des tâches répétitives. Utilisant une approche simple basée sur le langage YAML, il permet aux développeurs et aux administrateurs système de décrire les états souhaités de leurs environnements informatiques de manière claire et logique. Contrairement à d'autres outils de gestion de configuration, Ansible ne nécessite pas d'agent spécial installé sur les nœuds gérés : il fonctionne en se connectant via SSH, rendant son déploiement rapide et sécurisé. Ansible est particulièrement apprécié pour sa facilité d'utilisation, sa flexibilité et sa capacité à automatiser complexe infrastructures avec précision.

## Contexte
Dans ce lab, nous allons procéder à l'installation d'Ansible sous Ubuntu sur une instance cible crée sur AWS.

## Objectifs

- Installation d'Ansible sur une machine hôte Ubuntu 22.04 LTS.
- Création d'une seconde instance EC2 sur AWS qui servira de machine cliente. Elle sera configurée à distance depuis la machine hôte.

## Prérequis
Avoir une machine avec une installation préalable d'Ubuntu.

Dans notre cas, nous allons provisionner une instance EC2 fonctionnant sous Ubuntu grâce à AWS, sur laquelle nous effectuerons toutes nos configurations.

Documentation complémentaire.
[Comment créer un compte aws](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws)
[lancer une instance ec2 sur aws à l'aide de terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## Installation d'Ansible
Une fois connectés à notre instance EC2, qui nous servira de machine hôte à partir de laquelle nous allons piloter Ansible, nous allons procéder à la mise à jour du système et ensuite à l'installation proprement dite d'Ansible.
NB : dans le cas où l'installation d'Ansible ne fonctionne pas, il est possible que vous deviez installer préalablement les paquets suivants :

une fois connecté à l'instance EC2
````bash
ssh -i ssh_key.pem ubuntu@my_ip_address
````

nous allons exécuter les commandes suivante :

````bash
sudo apt update -y
sudo apt install software-properties-common
sudo apt install python3-pip -y
sudo pip3 install ansible
````

1. Commande d'installation **Ansible** sous ubuntu

````bash
sudo apt install ansible -y
````

>![alt text](img/image.png)
*Lancement de l'installation d'Ansible*

2. Vérification de la présence d'Ansible sur le serveur

   >![alt text](img/image-1.png)
   *Contrôle de la version d'Ansible*

La version qui a été installée est la 2.10.8, qui est la version la plus stable au moment de la réalisation de ce lab.

3. Création d'une seconde instance **"client"** qui servira à réaliser les configurations à distance, ceci depuis notre machine hôte **Ansible** créée précédemment.

[Procédure de création d'une instace ec2](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)